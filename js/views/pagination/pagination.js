define(['backbone'], function(Backbone) {
	var paginationView = Backbone.View.extend({
		events: {

		},

		initialize: function(options) {
			this.routePath = options.routePath;
			currentPage = options.currentPage || 1;
			totalPage = options.totalPage;

			recordsLength = options.recordsLength;

			this.countPerTen = Math.floor(currentPage/10);			
			this.floorTotalPage = (Math.floor(totalPage/10));


			if(currentPage%10==0) {
				this.countPerTen = this.countPerTen - 1;
			}

		},

		render: function() {	
			var docFrag = document.createDocumentFragment(),
				ul = document.createElement("ul");

			ul.setAttribute('class', 'pagination');
			if(totalPage > 0) {
				if(currentPage > 10 ) {
					ul.appendChild(this.pageButton((this.countPerTen*10) , '<<'));
					docFrag.appendChild(ul);
				}

				if(1+(this.countPerTen*10) <= totalPage) {
					ul.appendChild(this.pageButton(1+(this.countPerTen*10) , 1+(this.countPerTen*10)));
					docFrag.appendChild(ul);
				}

				if(2+(this.countPerTen*10) <= totalPage) {
					ul.appendChild(this.pageButton(2+(this.countPerTen*10) , 2+(this.countPerTen*10)));
					docFrag.appendChild(ul);
				}

				if(3+(this.countPerTen*10) <= totalPage) {
					ul.appendChild(this.pageButton(3+(this.countPerTen*10) , 3+(this.countPerTen*10)));
					docFrag.appendChild(ul);
				}

				if(4+(this.countPerTen*10) <= totalPage) {
					ul.appendChild(this.pageButton(4+(this.countPerTen*10) , 4+(this.countPerTen*10)));
					docFrag.appendChild(ul);
				}

				if(5+(this.countPerTen*10) <= totalPage) {
					ul.appendChild(this.pageButton(5+(this.countPerTen*10) , 5+(this.countPerTen*10)));
					docFrag.appendChild(ul);
				}

				if(6+(this.countPerTen*10) <= totalPage) {
					ul.appendChild(this.pageButton(6+(this.countPerTen*10) , 6+(this.countPerTen*10)));
					docFrag.appendChild(ul);
				}

				if(7+(this.countPerTen*10) <= totalPage) {
					ul.appendChild(this.pageButton(7+(this.countPerTen*10) , 7+(this.countPerTen*10)));
					docFrag.appendChild(ul);
				}

				if(8+(this.countPerTen*10) <= totalPage) {
					ul.appendChild(this.pageButton(8+(this.countPerTen*10) , 8+(this.countPerTen*10)));
					docFrag.appendChild(ul);
				}

				if(9+(this.countPerTen*10) <= totalPage) {
					ul.appendChild(this.pageButton(9+(this.countPerTen*10) , 9+(this.countPerTen*10)));
					docFrag.appendChild(ul);
				}

				if(10+(this.countPerTen*10) <= totalPage) {
					ul.appendChild(this.pageButton( 10+(this.countPerTen*10), 10+(this.countPerTen*10)));
					docFrag.appendChild(ul);
				}

				if( totalPage > 10+(this.countPerTen*10) ) {
					ul.appendChild(this.pageButton(11+(this.countPerTen*10) , '>>'));
					docFrag.appendChild(ul);
				}

			}

			this.$el.html(docFrag);

		},

		pageButton: function(page, text) {
			var li = document.createElement("li"),
				a = document.createElement("a");

			var route = this.routePath.concat(page);

			a.setAttribute('href', route);
			a.textContent = text;
			li.appendChild(a);
			
			return li;
		}
	});

	return paginationView;
});