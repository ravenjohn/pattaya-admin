define([
	'jquery',
	'underscore',
	'backbone',
	'validation'
], function($, _, Backbone, Validation){

	var choiceModal = Backbone.View.extend({
		id: 'base-modal',
        className: 'modal fade',
		events: {
			'hidden.bs.modal'				: 'teardown',
			'click .save'					: 'save',
		},

		initialize: function(model, options) {
			this.model = model;
			this.render();

			Validation.bind(this);
		},

		show: function() {
			this.$el.modal({show:true});
		},

		teardown: function() {
			this.$el.data('bs.modal', null);
			this.remove();
		},

		render: function(){
			var compiledTemplate = _.template( Application.Templates.UpdateChoice );
			this.$el.html(compiledTemplate);

			return this;			
		},

		save: function() {
			var values = {},
				error = 0;
			var $inputs = $('form :input', this.el);
			$inputs.each(function() {
				// values[this.id] = $(this).val();
				values[this.id] = ($(this).val()).replace(/[^\w.\s]/gi, '');
			});

			var self = this;
			var isNew = this.model.isNew();			
			var subCat = this.model.get('subcategories');

			values['price'] = values['price'] || 0;
			subCat.push(values);

			var name = $("#name", this.$el),
				price = $('#price', this.$el);

			if(name.val() == '') {
				error = 1;
				name.addClass('has-error');
			} else {
				name.removeClass('has-error');
			}

			if(isNaN(price.val()) || price.val() < 0) {
				error = 1;
				price.addClass('has-error');
			} else {
				price.removeClass('has-error');
			}


			if(!error) {
				this.model.save({
					subcategories: subCat
				}, {
					success: function(data, response, xhr) {
						self.model.trigger('change');
						self.$el.modal('hide');
					},
					error: function() {

					}
				});
			}


		},
	});

	return choiceModal;
});
