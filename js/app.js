define([
	'jquery',
	'underscore',
	'backbone',
	'router',

	//Models
	'models/transaction',
	'models/product',
	'models/promo',
	'models/reservation',
	'models/archive',
	'models/reward_setting',

	// Collections
	'collections/transaction',
	'collections/product',
	'collections/promo',
	'collections/reservation',
	'collections/archive',
	'collections/transactions',
	'collections/sale',
	'collections/order_count',
	'collections/inquiry',
	'collections/reward_settings',

	//Views
	'views/modal/base',
	'views/pagination/pagination',
	'views/transaction/transactionList',
	'views/transaction/transactionListItem',
	'views/transaction/transactionProductList',
	'views/product/productList',
	'views/product/productListitem',
	'views/promo/promoList',
	'views/promo/promoListItem',
	'views/reservation/reservationList',
	'views/reservation/reservationListItem',
	'views/archive/archiveList',
	'views/archive/archiveListItem',
	'views/archive/archiveProductItem',
	'views/promo/promoModal',
	'views/addOns/addOnsModal',
	'views/choice/choiceModal',
	'views/choice/choiceList',
	'views/choice/choiceListItem',
	'views/addOns/addOnsList',
	'views/addOns/addOnsListItem',
	'views/report/report',
	'views/inquiry/inquiryList',
	'views/inquiry/inquiryListItem',
	'views/rewardSettings/rewardSettings',
	'views/pay_on_table/payOnTableList',

	// Templates
	'text!templates/transaction/transactionList.html',
	'text!templates/transaction/transactionListItem.html',
	'text!templates/transaction/transactionProductItem.html',
	'text!templates/product/productList.html',
	'text!templates/product/productListItem.html',
	'text!templates/product/update.html',
	'text!templates/promo/update.html',
	'text!templates/promo/promoList.html',
	'text!templates/promo/promoListItem.html',
	'text!templates/reservation/reservationList.html',
	'text!templates/reservation/reservationListItem.html',
	'text!templates/reservation/update.html',
	'text!templates/archive/archiveList.html',
	'text!templates/archive/archiveListItem.html',
	'text!templates/archive/archiveProductItem.html',
	'text!templates/addOns/update.html',
	'text!templates/choice/update.html',
	'text!templates/choice/choiceList.html',
	'text!templates/choice/choiceListItem.html',
	'text!templates/addOns/addOnsList.html',
	'text!templates/addOns/addOnsListItem.html',
	'text!templates/report/report.html',
	'text!templates/inquiry/inquiryList.html',
	'text!templates/inquiry/inquiryListItem.html',
	'text!templates/rewardSettings/rewardSettings.html'
], function(
	$,
	_,
	Backbone,
	Router,

	//Models
	transactionModel,
	productModel,
	promoModel,
	reservationModel,
	archiveModel,
	rewardSettingModel,

	// Collections
	transactionCollection,
	productCollection,
	promoCollection,
	reservationCollection,
	archiveCollection,
	transactionsCollection,
	saleCollection,
	orderCollection,
	inquiryCollection,
	rewardSettingsCollection,

	//Views
	modalView,
	paginationView,
	transactionListView,
	transactionListItemView,
	transactionProductListView,
	productListView,
	productListItemView,
	promoListView,
	promoListItemView,
	reservationListView,
	reservationListItemView,
	archiveListView,
	archiveListItemView,
	archiveProductListView,
	promoModalView,
	addOnsModalView,
	choiceModalView,
	choiceListView,
	choiceListItemView,
	addOnsListView,
	addOnsListItemView,
	reportView,
	inquiryListView,
	inquiryListItemView,
	rewardSettingsView,
	payOnTableListView,

	//Templates
	transactionList,
	transactionListItem,
	transactionProductItem,
	productList,
	productListItem,
	updateProduct,
	updatePromo,
	promoList,
	promoListItem,
	reservationList,
	reservationListItem,
	updateReservation,
	archiveList,
	archiveListItem,
	archiveProductItem,
	updateAddOns,
	updateChoice,
	choiceList,
	choiceListItem,
	addOnsList,
	addOnsListItem,
	report,
	inquiryList,
	inquiryListItem,
	rewardSettings
) {

	Backbone.View.prototype.close = function () {
		this.unbind();
		this.undelegateEvents();
	};

	Date.prototype.UnixToDate = function(UNIX_timestamp) {
		var a = new Date(UNIX_timestamp*1000);
		var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		var year = a.getFullYear();
		var month = months[a.getMonth()];
		var date = a.getDate();
		var hour = a.getHours();
		var min = a.getMinutes();
		var sec = a.getSeconds();
		var time = month +' '+ date+','+year+' '+hour+':'+min+':'+sec ;
		return time;
	};


	var app = Backbone.View.extend({
		Models: {
			'Transaction'				: transactionModel,
			'Product'					: productModel,
			'Promo'						: promoModel,
			'Reservation'				: reservationModel,
			'Archive'					: archiveModel,
			'RewardSetting'				: rewardSettingModel
		},

		Collections: {
			'Transaction'				: transactionCollection,
			'Product'					: productCollection,
			'Promo'						: promoCollection,
			'Reservation'				: reservationCollection,
			'Archive'					: archiveCollection,
			'Transactions'				: transactionsCollection,
			'Sale'						: saleCollection,
			'OrderCount'				: orderCollection,
			'Inquiry'					: inquiryCollection,
			'RewardSetting'					: rewardSettingsCollection
		},

		Views: {
			'Modal'						: modalView,
			'Pagination'				: paginationView,
			'TransactionListView'		: transactionListView,
			'transactionListItemView'	: transactionListItemView,
			'TransactionProductListView': transactionProductListView,
			'ProductListView'			: productListView,
			'ProductListItemView'		: productListItemView,
			'PromoListView'				: promoListView,
			'PromoListItemView'			: promoListItemView,
			'ReservationListView'		: reservationListView,
			'ReservationListItemView'	: reservationListItemView,
			'ArchiveListView'			: archiveListView,
			'ArchiveListItemView'		: archiveListItemView,
			'ArchiveProductListView'	: archiveProductListView,
			'PromoModal'				: promoModalView,
			'AddOnsModal'				: addOnsModalView,
			'ChoiceModal'				: choiceModalView,
			'ChoiceListView'			: choiceListView,
			'ChoiceListItemView'		: choiceListItemView,
			'AddOnsListView'			: addOnsListView,
			'AddOnsListItemView'		: addOnsListItemView,
			'ReportView'				: reportView,
			'InquiryListView'			: inquiryListView,
			'InquiryListItemView'		: inquiryListItemView,
			'RewardSettingsView'		: rewardSettingsView,
			'PayOnTableListView'		: payOnTableListView
		},

		Templates: {
			'TransactionList'			: transactionList,
			'TransactionListItem'		: transactionListItem,
			'TransactionProductItem'	: transactionProductItem,
			'ProductList'				: productList,
			'ProductListItem'			: productListItem,
			'UpdateProduct'				: updateProduct,
			'UpdatePromo'				: updatePromo,
			'PromoList'					: promoList,
			'PromoListItem'				: promoListItem,
			'ReservationList'			: reservationList,
			'ReservationListItem'		: reservationListItem,
			'UpdateReservation'			: updateReservation,
			'ArchiveList'				: archiveList,
			'ArchiveListItem'			: archiveListItem,
			'ArchiveProductItem'		: archiveProductItem,
			'UpdateAddOns'				: updateAddOns,
			'UpdateChoice'				: updateChoice,
			'ChoiceList'				: choiceList,
			'ChoiceListItem'			: choiceListItem,
			'AddOnsList'				: addOnsList,
			'AddOnsListItem'			: addOnsListItem,
			'Report'					: report,
			'InquiryList'				: inquiryList,
			'InquiryListItem'			: inquiryListItem,
			'RewardSettings'			: rewardSettings
		}
	});


	var initialize = function() {
		window.Application = new app();
		window.base_url = 'http://pattayav2.mitpapps.com';
		window.pendingPickupBadgeCount = 0;
		window.pendingDeliveryBadgeCount = 0;
		window.pendingPayTableBadgeCount = 0;
		window.servedCashPayTableBadgeCount = 0;


		Router.initialize();
	};

	return {
		initialize: initialize
	}

});
