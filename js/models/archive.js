define(['backbone'], function(Backbone) {
	var archiveModel = Backbone.Model.extend({
		idAttribute: '_id',
		initialize: function(options) {
			this.options = options;
		},
		urlRoot: function() {
			return base_url +'/archive';
		}
	});

	return archiveModel;
});
