define(['backbone', 'bootstrapSelect'], function(Backbone) {
	var productList = Backbone.View.extend({
		el: '#content-container',

		events: {
			'click .createProduct'			: 'createProduct',
			'change .selectpicker'			: 'changeCategory'
		},

		initialize: function(options) {
			this.options = options;
			this.collection.on('add', this.addOne, this);
		},

		render: function() {
			this.$el.html( _.template(Application.Templates.ProductList) );

			//getAllCategories
			var self = this;
			$.ajax({
				url: base_url + '/food/categories',
				type: "GET",
				async: false,
				success: function(response, options) {
					var picker = "<select class=\"selectpicker\" name=\"categories\" data-style=\"btn-primary\" data-width=\"auto\"><option value=\"none\">All</option>";
					_.each(response, function(category) {
						picker += '<option value="'+category+'">'+category+'</option>';
					});
					picker += '</select>';

					$('.productPicker', self.$el).append(picker);

				},
				error: function() {
					console.log('error');
				}
			});

			$('.selectpicker', this.$el).selectpicker();

			if(retrieveCookie('productCategory') != undefined) {
				var text = $("select[name=selValue] option[value='1']").text(retrieveCookie('productCategory'));
				$('.bootstrap-select .filter-option').text(retrieveCookie('productCategory'));
				$('select[name=categories]').val(1);
			}


			this.addAll();
		},

		addAll: function() {
			var self = this,
				models = this.options.collection.models;

			_.each(models, function(model) {
				self.addOne(model);
			});

			var pagination = new Application.Views.Pagination({currentPage: this.options.current_page, totalPage: this.options.totalPage, recordsLength: this.options.recordsLength, routePath: '#food/'});
			pagination.render();

			$('.product-pagination', this.$el).html(pagination.$el);
			$('.product-pagination ul>li a[href="#food/'+this.options.current_page+'"]', this.$el).parent().addClass('active');
		},

		addOne: function(model) {
			var item = new Application.Views.ProductListItemView({model: model});
			item.render();

			$('#productTable > tbody', this.$el).append(item.$el);
		},

		createProduct: function(evt) {
			evt.preventDefault();

			var model = new Application.Models.Product();
			var Modal = new Application.Views.Modal(model, {template: Application.Templates.UpdateProduct, collection: this.collection})
			Modal.show();
		},

		changeCategory: function(ev) {
			ev.preventDefault();
			var value = $(ev.target).val(),
				self = this;

			var products = new Application.Collections.Product({page: 1});
			if(value == 'none') {
				removeCookie('productCategory', {path:'/'});
				products.fetch({
					success: function(collection, response, xhr) {
						self.initialize({collection: collection, totalPage: response.totalPage, current_page:1, recordsLength: response.records.length});
						self.render();
					}
				});
			} else {
				createCookie('productCategory', value, {path:'/'});
				products.fetch({
					data: {
						category: retrieveCookie('productCategory')
					},
					success: function(collection, response, xhr) {
						self.initialize({collection: collection, totalPage: response.totalPage, current_page:1, recordsLength: response.records.length});
						self.render();
					}
				});
			}
		}
	});

	return productList;
});