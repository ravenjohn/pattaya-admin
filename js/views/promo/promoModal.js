define([
	'jquery',
	'underscore',
	'backbone',
	'text!templates/promo/update.html',
	'validation'
], function($, _, Backbone, BaseModalTemplate, Validation){

	var BaseModalView = Backbone.View.extend({
		template: BaseModalTemplate,
		id: 'base-modal',
        className: 'modal fade',
		events: {
			'hidden.bs.modal'				: 'teardown',
			'click .save'					: 'save',
		},
		initialize: function(model, options) {
			this.model = model;
			this.collection = options.collection;
			this.options = options;
			this.render();

			Validation.bind(this);
		},
		show: function() {
			this.$el.modal({show:true});
		},
		teardown: function() {
			this.$el.data('bs.modal', null);
			this.remove();
		},
		render: function(){
			var compiledTemplate = _.template(this.template, {model: this.model.toJSON(), options: this.options});
			this.$el.html(compiledTemplate);


			return this;
		},
		save: function() {
			var $inputs = $('form :input', this.el);
			var values = {};
			$inputs.each(function() {
				$("label[for='"+this.id+"']").find("span").text('');

				if($(this).hasClass('has-error'))
					$(this).removeClass('has-error');

				if(this.type == 'datetime-local' || this.type == 'date' || this.type == 'datetime') {
					var date = $(this).val();
					values[this.id] = date.replace('T', ' ');
				} else {
					values[this.id] = ($(this).val()).replace(/[^\w.\s]/gi, '');
				}

			});


			var self = this,
				isNew = this.model.isNew(),
				errors = this.model.preValidate(values);


			if(errors)
			{
				for(key in errors) {
					$("label[for='"+key+"']").find("span").text(errors[key]);
					$("#"+key).addClass('has-error');
				}
			}
			else
			{
				if(values.validity > new Date().toJSON().slice(0,10)) {
					this.model.save(values, {
						success: function(result, model) {
							self.$el.modal('hide');
							if (isNew) {
								// self.collection.add(model);
							}
						},
						error: function(result, response) {

						}
					});
				} else {
					$('#validity', this.$el).addClass('has-error');
				}
			}



		},
	});
	return BaseModalView;
});
