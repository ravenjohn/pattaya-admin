define(['backbone'], function(Backbone) {
	var choiceList = Backbone.View.extend({
		events: {

		},

		initialize: function(options) {
			this.options = options;
		},

		render: function() {
			var template = _.template( Application.Templates.ChoiceList);
			this.$el.html(template);

			this.addAll();
		},

		addAll: function() {
			var categories = this.options.subcategories,
				self = this;

			_.each(categories, function(category) {
				self.addOne(category);
			});
		},

		addOne: function(category) {
			var choiceListItem = new Application.Views.ChoiceListItemView({model: this.options.model, category: category});
			choiceListItem.render();

			$('.table tbody', this.$el).append(choiceListItem.$el);		
		}


	});

	return choiceList;
});