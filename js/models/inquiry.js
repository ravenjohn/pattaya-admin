define(['backbone'], function(Backbone) {
	var model = Backbone.Model.extend({
		defaults: {
			email: '',
			phone: '',
			message: ''
		}
	});

	return model;
});