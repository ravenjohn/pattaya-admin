define([
	'jquery',
	'underscore',
	'backbone',
	'validation'
], function($, _, Backbone, Validation){

	var choiceModal = Backbone.View.extend({
		id: 'base-modal',
        className: 'modal fade',
		events: {
			'hidden.bs.modal'				: 'teardown',
			'click .save'					: 'save',
		},

		initialize: function(model, options) {
			this.model = model;
			this.render();

			Validation.bind(this);
		},

		show: function() {
			this.$el.modal({show:true});
		},

		teardown: function() {
			this.$el.data('bs.modal', null);
			this.remove();
		},

		render: function(){
			var compiledTemplate = _.template( Application.Templates.UpdateAddOns );
			this.$el.html(compiledTemplate);

			return this;			
		},

		save: function(evt) {
			var thisBtn = $(evt.currentTarget, this.$el);
			thisBtn.addClass('disabled');

			var values = {},
				error = 0;
			var $inputs = $('form :input', this.el);
			$inputs.each(function() {
				// values[this.id] = $(this).val();
				values[this.id] = ($(this).val()).replace(/[^\w.\s]/gi, '');
			});

			var self = this;
			var isNew = this.model.isNew();			
			var addOns = this.model.get('addons');
			values['price'] = values['price'] || '0';

			var name = $("#name", this.$el);
			var price = $('#price', this.$el);

			if(name.val() === '') {
				error = 1;
				name.addClass('has-error');
			} else {
				name.removeClass('has-error');
				error = 0;
			}

			if(price.val() === '' || price.val() <= 0 || isNaN(price.val())) {
				error = 1;
				price.addClass('has-error');
			} else {
				error = 0;
				price.removeClass('has-error');
			}


			if(!error) {
				addOns.push(values);
				this.model.save({
					addons: addOns
				}, {
					success: function(data, response, xhr) {
						self.model.trigger('change');
						self.$el.modal('hide');
						if(thisBtn.hasClass('disabled'))
							thisBtn.removeClass('disabled');
					},
					error: function() {
						if(thisBtn.hasClass('disabled'))
							thisBtn.removeClass('disabled');
					}
				});
			} else {
				if(thisBtn.hasClass('disabled'))
					thisBtn.removeClass('disabled');
			}

		},
	});

	return choiceModal;
});
