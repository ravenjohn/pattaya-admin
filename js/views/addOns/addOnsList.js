define(['backbone'], function(Backbone) {
	var addOnsList = Backbone.View.extend({
		initialize: function(options) {
			this.options = options;
		},

		render: function() {
			var template = _.template( Application.Templates.AddOnsList );
			this.$el.html(template);

			this.addAll();
		},

		addAll: function() {
			var self = this,
				addOns = this.options.addons;

			_.each(addOns, function(addons) {
				self.addOne(addons);
			});
		},

		addOne: function(addon) {
			var addOnsListItem = new Application.Views.AddOnsListItemView({model: this.model, addOns: addon});
			addOnsListItem.render();

			$('.table tbody', this.$el).append(addOnsListItem.$el);
			
		}
	});

	return addOnsList;
});	