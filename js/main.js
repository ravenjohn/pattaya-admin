require.config({
	paths: {
		jquery 				: "../assets/jquery/jquery.min",
		underscore 			: "../assets/underscore/underscore-min",
		backbone 			: "../assets/backbone/backbone.min",
		bootstrap 			: "../assets/bootstrap/bootstrap.min",
		validation			: "../assets/backbone/validation/backbone.validation.min",
		toast				: "../assets/toast/jquery.toastmessage",
		nprogress			: "../assets/nprogress/nprogress",
		bootbox				: "../assets/bootbox/bootbox.min",
		bootstrapSelect		: "../assets/bootstrap/bootstrap-select.min",
		cookie				: "../assets/javascript-cookie"
	},


	shim: {
		underscore : {
			deps: ['jquery'],
			exports: '_'
		},
		backbone: {
			deps: ["jquery","underscore"],
			exports: "Backbone"
		},
		bootstrap: {
			deps: ['jquery'],
			exports: "$.fn.popover"
		},
		validation: {
			deps: ['underscore', 'backbone'],
			exports: "Backbone.Validation"
		},
		nprogress: {
			deps: ['jquery'],
			exports: "NProgress"
		},
		toast: {
			deps: ['jquery'],
			exports: "$.fn.toastmessage"
		},
		bootbox: {
			deps: ['jquery']
		},
		bootstrapSelect: {
			deps: ['jquery', 'bootstrap']
		},
	}
});


// define(['jquery', 'underscore'], function(jquery, underscore) {
	require(['app','bootstrap'], function(App) {
		App.initialize();
	});
// });