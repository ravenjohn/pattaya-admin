define([
	'backbone',
	'text!templates/pay_on_table/payOnTableList.html',
	'views/pay_on_table/payOnTableListItem'
	], function(Backbone, payOnTableListTemplate, payOnTableListItemView) {
	var view = Backbone.View.extend({
		el: '#content-container',
		initialize: function(options) {
			this.options = options;
		},
		render: function() {
			this.$el.html( _.template(payOnTableListTemplate, {status: this.options.status}) );

			this.addAll();
		},
		addAll: function() {
			var self = this,
				models = this.options.collection.models;

			if(!models.length) $('table.table tbody', this.$el).append('<tr><td>Pay on Table is Empty</td></tr>');
			_.each(models, function(model) {
				self.addOne(model);
			});
		},
		addOne: function(model) {
			var item = new payOnTableListItemView({model: model, status: this.options.status});
			item.render();

			$('table.table tbody', this.$el).append( item.$el );
		}
	});

	return view;
});