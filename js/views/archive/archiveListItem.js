define(['backbone'], function(Backbone) {
	var transactionListItem = Backbone.View.extend({
		tagName: 'tr',

		events: {

		},

		initialize: function(options) {
			this.options = options;
		},

		render: function() {
			this.$el.html( _.template(Application.Templates.ArchiveListItem, this.model.toJSON()) );

			this.addAll();
		},

		addAll: function() {
			var model = this.model
				self = this;

			var food_item = model.get('transaction_items');
			_.each(food_item, function(item) {
				self.addOne(item);
			});
		},

		addOne: function(food) {
			var food = new Application.Views.ArchiveProductListView({model: food});
			food.render();

			$('.transactionItem', this.$el).append( food.$el );
		}

	});

	return transactionListItem;
});