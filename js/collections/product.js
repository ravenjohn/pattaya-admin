define(['backbone', 'models/product'], function(Backbone, productModel) {
	var productCollection = Backbone.Collection.extend({
		model: productModel,
		initialize: function(options) {
			this.options = options;
		},
		url: function() {
			return base_url+"/food/?page="+this.options.page;
		},
		parse: function(response) {
			return response.records;
		}
	});

	return productCollection;
});