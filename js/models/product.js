define(['backbone'], function(Backbone) {
	var productModel = Backbone.Model.extend({
		idAttribute: '_id',
		initialize: function(options) {
			this.options = options;
		},
		urlRoot: function() {
			return base_url +'/food';
		},
		defaults: {
			name: '',
			short_description: '',
			description: '',
			type: '',
			category: '',
			flavor: '',
			price: '',
			subcategories: [],
			addons: []
		},
		validation: function() {
			return {
				name: {
					required: true,
					msg: 'Required'
				},
				short_description: {
					required: false,
					msg: 'Required'
				},
				description: {
					required: false,
					msg: 'Required'
				},
				type: {
					required: true,
					msg: 'Required'
				},
				category: {
					required: true,
					msg: 'Required'
				},
				price: [
				{
					required: true,
					msg: 'Required'
				},
				{
					pattern: 'number',
					msg: 'numeric only'
				}
				],
				flavor: {
					required: false
				},
			};
		}
	});

	return productModel;
});