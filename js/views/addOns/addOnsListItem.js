define(['backbone'], function(Backbone) {
	var addOnsListItem = Backbone.View.extend({
		tagName: 'tr',
		events: {
			"click .deleteAddOns"		: "deleteAddOns"
		},
		initialize: function(options) {
			this.options = options;
		},
		render: function() {
			var template = _.template( Application.Templates.AddOnsListItem, this.options.addOns );
			this.$el.html(template);
		},

		deleteAddOns: function(evt) {
			evt.preventDefault();

			var model = this.options.model,
				self = this;

			for(i=0; i<model.get('addons').length; i++) {
				var addOns = model.get('addons')[i];
				if( JSON.stringify(addOns) === JSON.stringify(self.options.addOns)  ) {
					var addons = model.get('addons').splice(i, 1);

					model.save({addons: model.get("addons")}, {
						success: function(data, response, XHR) {
							$(self.$el).fadeOut();
						},
						error: function() {

						}
					});
					
				}
			}
		}
	});

	return addOnsListItem;
});