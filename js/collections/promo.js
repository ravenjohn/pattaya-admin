define(['backbone', 'models/promo'], function(Backbone, promoModel) {
	var promo = Backbone.Collection.extend({
		model: promoModel,
		initialize: function(options) {
			this.options = options;
		},
		url: function(response) {
			return base_url + '/promo/?page='+this.options.page;
		},
		parse: function(response) {
			return response.records;
		}
	});

	return promo;
});