define(['backbone'], function(Backbone) {
	var promoListItem = Backbone.View.extend({
		tagName: 'tr',
		events: {
			'click .delete'		: 'delete'
		},	
		initialize: function(options) {
			this.options = options;
		},
		render: function() {
			this.$el.html( _.template(Application.Templates.PromoListItem, this.model.toJSON()) );
		},

		delete: function(evt) {
			evt.preventDefault();
			var self = this;
			if(self.model.destroy()) {
				$(self.$el).fadeOut();
			}
		}
	});

	return promoListItem;
});