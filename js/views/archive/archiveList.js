define(['backbone'], function(Backbone) {
	var archiveList = Backbone.View.extend({
		el: '#content-container',
		events: {
		},
		initialize: function(options) {
			this.options = options;
			var sale = new Application.Collections.Sale(),
				self = this,
				date = new Date();
		},
		render: function() {
			this.$el.html( _.template(Application.Templates.ArchiveList) );

			this.addAll();
		},

		addAll: function() {
			var self = this,
				models = this.options.collection.models;


			
			if(!models.length) $('.table tbody', this.$el).append('<tr><td>Archive is Empty</td></tr>');

			_.each(models, function(model) {
				self.addOne(model);
			});

			var pagination = new Application.Views.Pagination({currentPage: this.options.current_page, totalPage: this.options.totalPage, recordsLength: this.options.recordsLength, routePath: '#archive/'});
			pagination.render();

			$('.archive-pagination', this.$el).html(pagination.$el);
			$('.archive-pagination ul>li a[href="#archive/'+this.options.currentPage+'"]', this.$el).parent().addClass('active');
		},

		addOne: function(model) {
			var listItem = new Application.Views.ArchiveListItemView({model: model});
			listItem.render();

			$('.table tbody', this.$el).append(listItem.$el);
		},

		changeMonthSale: function(evt) {
			evt.preventDefault();

			var self = this,
				sale = new Application.Collections.Sale();

			sale.fetch({
				data: {
					created_month : $('#month', self.$el).val(),
					created_year: new Date().getFullYear(),
					status: 'accepted'
				},
				success: function(collection, response, xhr){
					$('.monthSale', self.$el).text((response.total_sale).toFixed(2));
				},
				error: function() {

				}
			});
		}

	});

	return archiveList;
});