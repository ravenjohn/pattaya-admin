define(['backbone'], function(Backbone) {
	var reservation = Backbone.Model.extend({
		idAttribute: '_id',
		initialize: function(options) {
			this.options = options;
		},
		urlRoot: function() {
			return base_url +'/reservation';
		},
		defaults: {
			name: '',
			email: '',
			number_of_person: '',
			contact_number: '',
			reservation_time: new Date().toJSON().slice(0,19)
		},
		validation: function() {
			return {
				name: {
					required: true,
					msg: 'Required'
				},
				number_of_person: [
				{
					required: true,
					msg: 'Required'
				},
				{
					min: 1,
					msg: 'invalid'
				}
				],
				contact_number: {
					required: true,
					msg: 'Required'
				},
				email: [
				{
					required: true,
					msg: 'Required'
				},
				{
					pattern: 'email',
					msg: 'invalid email format'
				}
				]
			};
		}
	});

	return reservation;
});