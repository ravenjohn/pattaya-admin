define(['backbone', 'models/archive'], function(Backbone, archiveModel) {
	var archiveCollection = Backbone.Collection.extend({
		model: archiveModel,
		initialize: function(options) {
			this.options = options;
		},
		url: function() {
			return base_url +'/archive/?page='+this.options.page;
		},
		parse: function(response) {
			return response.records;
		}
	});

	return archiveCollection;
});