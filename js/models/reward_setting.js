define(['backbone'], function(Backbone) {
	var rewardSettingModel = Backbone.Model.extend({
		idAttribute: '_id',
		initialize: function(options) {
			this.options = options;
		},
		urlRoot: function() {
			return base_url + '/reward_setting';
		},
		validation: function() {
			return {
				amount: [
				{
					pattern: 'number',
					msg: 'numeric only'
				},
				{
					required: true,
					msg: 'Amount is required'
				}
				],
				percentage : [
				{
					pattern : 'number',
					msg : 'numeric only'
				},
				{
					range : [1, 100],
					msg : 'limit of 1-100'
				},
				{
					required: true,
					msg: 'Percentage is required'
				}
				],
				status : {
					required : true,
					msg : 'Status is required'
				}
			};
		},
		defaults : {
			amount : 100,
			percentage : 5,
			status : 'deactivated'
		}
	});

	return rewardSettingModel;
});
