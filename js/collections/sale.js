define(['backbone'], function(Backbone) {
	var saleCollection = Backbone.Collection.extend({
		initialize: function(options) {
			this.options = options;
		},
		url: function() {
			return base_url + '/sale'
		}
	});

	return saleCollection;
});