define(['backbone', 'nprogress'], function(Backbone, NProgress) {
	var reservationListItem = Backbone.View.extend({
		tagName: 'tr',
		events: {
			'click .decline'		: 'declined',
			'click .accept'			: 'accepted',
			'click .done'			: 'done'
		},
		initialize: function(options) {
			this.options = options;
		},
		render: function() {
			this.$el.html( _.template(Application.Templates.ReservationListItem, this.model.toJSON()) );
		},

		declined: function(evt) {
			evt.preventDefault();
			NProgress.start();
			var self = this;
			if(this.model.get('status') !== 'accepted') {
				$.ajax({
					type :"put",
					url: base_url +'/reservations/'+self.model.id+'/decline',
					success: function(data, response, XHR) {
						if(data.Error) {
							NProgress.done();
							$().toastmessage('showToast', {
								text     : 'Decline failed',
								sticky   : false,
								type     : 'error'
							});
						} else {
							$(self.$el).fadeOut();
							NProgress.done();
							$().toastmessage('showToast', {
								text     : 'Successfully declined',
								sticky   : false,
								type     : 'success'
							});
						}
					},
					error: function(err) {
						console.log('STRIPE PAYMENT FAILED.');
					}
				});
			} else {
				if(this.model.destroy({data: { status: 'remove' }, processData: true})) {
					$(this.$el).fadeOut();
					NProgress.done();
					$().toastmessage('showToast', {
						text     : 'Reservation Remove',
						sticky   : false,
						type     : 'error'
					});
				}
			}
		},

		accepted: function(evt) {
			evt.preventDefault();
			NProgress.start();

			var self = this;
			$.ajax({
				type :"post",
				data: {
					amount: 100
				},
				url: base_url +'/reservations/'+self.model.id+'/accept',
				success: function(data, response, XHR) {
					if(data.Error) {
						NProgress.done();
						$().toastmessage('showToast', {
							text     : 'Payment Failed',
							sticky   : false,
							type     : 'error'
						});
					} else {
						$(self.$el).fadeOut();
						NProgress.done();
						$().toastmessage('showToast', {
							text     : 'Payment Successful',
							sticky   : false,
							type     : 'success'
						});
					}
				},
				error: function(err) {
					console.log('STRIPE PAYMENT FAILED.');
				}
			});
		},

		done: function(evt) {
			evt.preventDefault();
			var self = this;
			this.model.save({status: 'done'}, {
				success: function(data, response, xhr) {
					$(self.$el).fadeOut();
				}
			});
		}
	});

	return reservationListItem;
});