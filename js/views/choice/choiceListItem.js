define(['backbone'], function(Backbone) {
	var choiceListItem = Backbone.View.extend({
		tagName: 'tr',
		events: {
			"click .editChoice"		: "editChoice",
			"click .deleteChoice"	: "deleteChoice"
		},
		initialize: function(options) {
			this.options = options;
		},

		render: function() {
			var template = _.template( Application.Templates.ChoiceListItem, this.options.category );
			this.$el.html(template);
		},

		editChoice: function(evt) {
			evt.preventDefault();
			console.log('edit');
		},

		deleteChoice: function(evt) {
			evt.preventDefault();
			var model = this.options.model,
				self = this;

			for(i=0; i<model.get('subcategories').length; i++) {
				var category = model.get('subcategories')[i];
				if( JSON.stringify(category) === JSON.stringify(self.options.category)  ) {
					var subcategories = model.get('subcategories').splice(i, 1);

					model.save({subcategories: model.get("subcategories")}, {
						success: function(data, response, XHR) {
							$(self.$el).fadeOut();
						},
						error: function() {

						}
					});
					
				}
			}
		}

	});

	return choiceListItem;
});