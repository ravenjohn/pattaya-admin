define(['backbone', 'toast', 'validation'], function(Backbone, NProgress, Validation) {
	var rewardSettingsView =  Backbone.View.extend({
		el: '#content-container',

		initialize : function () {
			Validation.bind(this);
		},

		events: {
			'click #status' : 'toggle',
			'submit form' : 'save'
		},

		render: function() {
			this.$el.html( _.template(Application.Templates.RewardSettings, this.model.toJSON()) );
		},

		toggle : function (e) {
			var b = e.target;
			if (b.value=='Activated') {
				b.value = 'Deactivated';
				b.className = 'btn btn-danger';
			} else {
				b.value = 'Activated';
				b.className = 'btn btn-success';
			}
		},

		save : function (e) {
			var data = {
				amount : $('#amount').val(),
			    	percentage : $('#percentage').val(),
			   	status : $('#status').val()
			    },
			    errors = this.model.preValidate(data),
			    arr = [],
			    i;
			e.preventDefault();
			if (errors) {
				for (i in errors) {
					arr.push(i + ' - ' + errors[i]);
				}
				$().toastmessage('showToast', {
				        text     : arr.join(' and '),
				        sticky   : false,
				        type     : 'warning'
				});
				return false;
			}
			this.model.save(data, {
				error : function () {
					$().toastmessage('showToast', {
						text     : 'Save failed :(',
						sticky   : false,
						type     : 'warning'
					});
				},
				success : function () {
					$().toastmessage('showToast', {
						text     : 'Save successful',
						sticky   : false,
						type     : 'success'
					});
				}
			});
			return false;
		}
	});
	return rewardSettingsView;
});
