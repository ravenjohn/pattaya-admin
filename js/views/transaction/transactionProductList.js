define(['backbone'], function(Backbone) {
	var product = Backbone.View.extend({
		className: 'row',

		events: {
		},

		initialize: function(options) {
			this.options = options;
		},

		render: function() {
			var product_template = _.template( Application.Templates.TransactionProductItem , this.model);
			this.$el.html( product_template );
		}

	});

	return product;
});