define(['backbone', 'models/reservation'], function(Backbone, reservationModel) {
	var reservation = Backbone.Collection.extend({
		model: reservationModel,

		initialize: function(options) {
			this.options = options;
		},

		url: function() {
			return base_url + '/reservation/?page='+ this.options.page;
		},

		parse: function(response) {
			return response.records;
		}
	});

	return reservation;
});