define(['backbone'], function(Backbone) {
	var promoList = Backbone.View.extend({
		el: '#content-container',
		events: {
			
		},
		initialize: function(options) {
			this.options = options;
		},
		render: function() {
			this.$el.html( _.template(Application.Templates.PromoList) );

			this.addAll();
		},

		addAll: function() {
			var self = this,
				models = this.options.collection.models;

			_.each(models, function(model) {
				self.addOne(model);
			});

			var pagination = new Application.Views.Pagination({currentPage: this.options.current_page, totalPage: this.options.totalPage, recordsLength: this.options.recordsLength, routePath: '#promo/'});
			pagination.render();

			$('.promo-pagination', this.$el).html(pagination.$el);
			$('.promo-pagination ul>li a[href="#promo/'+this.options.current_page+'"]', this.$el).parent().addClass('active');
		},

		addOne: function(model) {
			var item = new Application.Views.PromoListItemView({model: model});
			item.render();

			$('.table tbody', this.$el).append(item.$el);
		}
	});

	return promoList;
})