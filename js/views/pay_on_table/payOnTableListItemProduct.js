define([
	'backbone',
	'text!templates/pay_on_table/payOnTableListItemProduct.html'
	], function(Backbone, payOnTableListItemProductTemplate) {
	var view = Backbone.View.extend({
		className: 'row',
		initialize: function(options) {
			this.options = options;
			this.model.instruction = this.model.instruction ? this.model.instruction : '';
		},
		render: function() {
			this.$el.html( _.template(payOnTableListItemProductTemplate, this.model) );
		}
	});

	return view;
});