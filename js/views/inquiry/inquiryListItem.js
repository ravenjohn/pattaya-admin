define(['backbone'], function(Backbone) {
	var listitem = Backbone.View.extend({
		tagName: 'tr',
		events: {

		},
		initialize: function(options) {
			this.options = options;
		},
		render: function() {
			this.$el.html( _.template(Application.Templates.InquiryListItem, this.model.toJSON()) );
		}
	});

	return listitem;
});