define(['backbone', 'cookie'], function(Backbone, jqueryCookie) {
	var Router = Backbone.Router.extend({
		routes: {
			''							: 'foodList',
			'pickup'					: 'pickupList',
			'pickup/:page'				: 'pickupList',
			'delivery'					: 'deliveryList',
			'delivery/:page'			: 'deliveryList',
			'pay_on_table/:status'		: 'payOnTableList',
			'pay_on_table/:status/page/:page' 	: 'payOnTableList',
			'food'						: 'foodList',
			'food/:page'				: 'foodList',
			'promo'						: 'promoList',
			'promo/:page'				: 'promoList',
			'reservation/:status'		: 'reservationList',
			'reservation/:status/:page'	: 'reservationList',
			'archive'					: 'archiveList',
			'archive/:page'				: 'archiveList',
			'report'					: 'report',
			'inquiry'					: 'inquiryList',
			'inquiry/:page'				: 'inquiryList',
			'settings'					: 'settings'
		},

		initialize: function (ev) {
			$('li a[href="#pay_on_table"]').click(function (e) {
				$('#cashAudio').attr('src', '');
			});
		},

		showView: function (view) {
	        //destroy current view
	        if(this.currentView !== undefined) {
	        	this.currentView.close();
	        }

	        //create new view
	        this.currentView = view;
	        this.currentView.delegateEvents();

	        return this.currentView;
	    }

	});

	var initialize = function(options) {
		var router = new Router();

		var counter = 0;
		var newCount = 0;
		var oldCount = 0;
		var self = this;

		var oldPendingDelivery = 0,
			newPendingDelivery = 0;

		var oldPendingPickup = 0,
			newPendingPickup = 0;

		var oldPendingPayTable = 0,
			newPendingPayTable = 0;

		var cashCount = 0;


		$.ajax({
			type: "GET",
			url: base_url + '/transaction_count/',
			async: false,
			success: function (response, data, xhr) {
				//Delivery
				newPendingDelivery = response.deliveryCount;
				pendingDeliveryBadgeCount = response.deliveryCount;

				if(pendingDeliveryBadgeCount > 0)
					$('ul.nav.navbar-nav li a[href="#delivery"] span.badge', self.$el).text(pendingDeliveryBadgeCount);
			
				//Pick-up
				newPendingPickup = response.pick_up;
				pendingPickupBadgeCount = response.pick_up;

				if(pendingPickupBadgeCount > 0)
					$('ul.nav.navbar-nav li a[href="#pickup"] span.badge', self.$el).text(pendingPickupBadgeCount);

			
				//Pay table
				// Pending
				pendingPayTableBadgeCount = response.pay_table_pendingCount;

				if(pendingPayTableBadgeCount > 0)
					$('ul.nav.navbar-nav li a[href="#pay_on_table"] span.badge', self.$el).text(pendingPayTableBadgeCount);
			
				// serve for cash
				servedCashPayTableBadgeCount = response.pay_table_served_cashCount;
				cashCount = response.pay_table_served_cashCount;

				if(cashCount > 0)
					$('ul.nav.navbar-nav li a span.cashBadge', self.$el).text(cashCount);
				
			}
		});


		setInterval(function(){
			var allTransactions = new Application.Collections.Transactions();
			allTransactions.fetch({
				success: function(collection, response, XHR) {
					newCount = collection.length;

					$.ajax({
						type: "GET",
						url: base_url + '/transaction_count/',
						async: false,
						success: function (response, data, xhr) {
							//Delivery
							newPendingDelivery = response.deliveryCount;
							pendingDeliveryBadgeCount = response.deliveryCount;

							if(pendingDeliveryBadgeCount > 0)
								$('ul.nav.navbar-nav li a[href="#delivery"] span.badge', self.$el).text(pendingDeliveryBadgeCount);

							//Pick-up
							newPendingPickup = response.pick_up;
							pendingPickupBadgeCount = response.pick_up;

							if(pendingPickupBadgeCount > 0)
								$('ul.nav.navbar-nav li a[href="#pickup"] span.badge', self.$el).text(pendingPickupBadgeCount);
						
							//Pay table
							// Pending
							pendingPayTableBadgeCount = response.pay_table_pendingCount;
							servedCashPayTableBadgeCount = response.pay_table_served_cashCount;

							if(pendingPayTableBadgeCount > 0)
								$('ul.nav.navbar-nav li a[href="#pay_on_table"] span.badge', self.$el).text(pendingPayTableBadgeCount);
						}
					});

					if(counter) {
						if(oldCount < newCount){

							var locHash = window.location.hash.split("/");
							var hash = locHash[0];
							var status = locHash[1];

							if( oldPendingPickup < newPendingPickup ) {
								$('ul.nav.navbar-nav li a[href="#pickup"]', self.$el).addClass('blink');
								oldPendingPickup = newPendingPickup;
							}

							if( oldPendingDelivery < newPendingDelivery ) {
								$('ul.nav.navbar-nav li a[href="#delivery"]', self.$el).addClass('blink');							
								oldPendingDelivery = newPendingDelivery;
							}

							if( oldPendingPayTable < newPendingPayTable ) {
								$('ul.nav.navbar-nav li a[href="#pay_on_table"]', self.$el).addClass('blink');
								oldPendingPayTable = newPendingPayTable;
							}

							$('#audio').attr('autoplay', 'autoplay').attr('loop', true).attr('src', 'media/ocean_liner.mp3');

							if(hash === '#delivery') {
								window.location.hash = '#delivery';

								var transactions = new Application.Collections.Transaction({page: 1});
								transactions.fetch({
									data: {
										type: 'delivery'
									},
									success: function(collection, response, xhr) {
										var transactionList = router.showView( new Application.Views.TransactionListView({collection: collection, totalPage: response.totalPage, current_page:1, recordsLength: response.records.length, type:'Delivery'}) );
										transactionList.render();
									}
								});
							} 
							else if(hash === '#pickup') {
								window.location.hash = '#pickup';

								var transactions = new Application.Collections.Transaction({page: 1});
								transactions.fetch({
									data: {
										type: 'pick_up'
									},
									success: function(collection, response, xhr) {
										var transactionList = router.showView( new Application.Views.TransactionListView({collection: collection, totalPage: response.totalPage, current_page:1, recordsLength: response.records.length, type:'Pick up'}) );
										transactionList.render();
									}
								});
							}
							else if(hash === '#pay_on_table') {
								window.location.hash = '#pay_on_table/'+ status;
								var transactions = new Application.Collections.Transaction({page: 1});
								transactions.fetch({
									data: {
										type: 'pay_table',
										status: status
									},
									success: function(collection, response, xhr) {
										var transactionList = router.showView( new Application.Views.PayOnTableListView({collection: collection, totalPage: response.totalPage, current_page:1, recordsLength: response.records.length, status: status}) );
										transactionList.render();
									}
								});
							}
						} else {
							oldPendingPickup = newPendingPickup;
							oldPendingDelivery = newPendingDelivery;
							oldPendingPayTable = newPendingPayTable;
						}

						//Sounds for cash pay table
						$.ajax({
							type: "GET",
							url: base_url + '/transaction/',
							async: false,
							data: {
								type: 'pay_table',
								status: 'served',
								mode_of_payment: 'cash'
							},
							success: function(collection, response, xhr) {
								if (cashCount < collection.length) {
									$('#cashAudio').attr('autoplay', 'autoplay').attr('loop', true).attr('src', 'media/bell.mp3');
									$('#audio').attr('src', '');

									if (collection.length > 0)
									$('ul.nav.navbar-nav li a span.cashBadge', self.$el).text(collection.length);
								}

								servedCashPayTableBadgeCount = collection.length;
								cashCount = collection.length;
							}
						});


					} else {
						counter++;
					}

					oldCount = newCount;
				},
				error: function() {

				}
			});
		},5000);


		router.on('route:pickupList', function(page) {
			var page = page || 1,
				self = this;
			var transactions = new Application.Collections.Transaction({page: page});
			transactions.fetch({
				data: {
					type: 'pick_up'
				},
				success: function(collection, response, xhr) {
					var transactionList = router.showView( new Application.Views.TransactionListView({collection: collection, totalPage: response.totalPage, current_page:page, recordsLength: response.records.length, type:'Pick up'}) );
					transactionList.render();
				}
			});

			$('.active').removeClass('active');
			$('[href="#pickup"]').parent().addClass('active');
			$('ul.nav.navbar-nav li a[href="#pickup"]', self.$el).removeClass('blink');

			if(pendingPickupBadgeCount > 0)
				$('ul.nav.navbar-nav li a[href="#pickup"] span.badge', self.$el).text(pendingPickupBadgeCount);
			else
				$('ul.nav.navbar-nav li a[href="#pickup"] span.badge', self.$el).text('');
		});

		router.on('route:deliveryList', function(page) {			
			var page = page || 1,
				self = this;
			var transactions = new Application.Collections.Transaction({page: page});
			transactions.fetch({
				data: {
					type: 'delivery'
				},
				success: function(collection, response, xhr) {
					var transactionList = router.showView( new Application.Views.TransactionListView({collection: collection, totalPage: response.totalPage, current_page:page, recordsLength: response.records.length, type:'Delivery'}) );
					transactionList.render();
				}
			});

			$('.active').removeClass('active');
			$('[href="#delivery"]').parent().addClass('active');
			$('ul.nav.navbar-nav li a[href="#delivery"]', self.$el).removeClass('blink');

			if(pendingDeliveryBadgeCount > 0)
				$('ul.nav.navbar-nav li a[href="#delivery"] span.badge', self.$el).text(pendingDeliveryBadgeCount);
			else
				$('ul.nav.navbar-nav li a[href="#delivery"] span.badge', self.$el).text('');
		});

		router.on('route:payOnTableList', function(status, page) {
			var page = page || 1,
				self = this;
			var transactions = new Application.Collections.Transaction({page: page});
			var data = {
				type: 'pay_table'
			};

			if (status === 'cash') {
				data.status = 'served';
				data.mode_of_payment = 'cash';
			} else if (status === 'served'){
				data.mode_of_payment = '';
				data.status = status;
			} else {
				data.status = status;
			}

			transactions.fetch({
				data: data,
				success: function(collection, response, xhr) {
					var transactionList = router.showView( new Application.Views.PayOnTableListView({collection: collection, totalPage: response.totalPage, current_page:page, recordsLength: response.records.length, status: status}) );
					transactionList.render();
				}
			});

			$('.active').removeClass('active');
			$('[href="#pay_on_table"]').parent().addClass('active');
			$('[href="#pay_on_table/'+status+'"]').parent().addClass('active');
			$('ul.nav.navbar-nav li a[href="#pay_on_table"]', self.$el).removeClass('blink');

			if(pendingPayTableBadgeCount > 0)
				$('ul.nav.navbar-nav li a[href="#pay_on_table"] span.badge.pending', self.$el).text(pendingPayTableBadgeCount);
			else
				$('ul.nav.navbar-nav li a[href="#pay_on_table"] span.badge.pending', self.$el).text('');
		

			if (servedCashPayTableBadgeCount > 0)
				$('ul.nav.navbar-nav li a span.cashBadge', self.$el).text(servedCashPayTableBadgeCount);
			else
				$('ul.nav.navbar-nav li a span.cashBadge', self.$el).text('');

		});

		router.on('route:foodList', function(page) {
			var page = page || 1;

			var products = new Application.Collections.Product({page: page});
			if(retrieveCookie('productCategory') != undefined) {
				products.fetch({
					data: {
						category: retrieveCookie('productCategory')
					},
					success: function(collection, response, xhr) {
						var productList = router.showView( new Application.Views.ProductListView({collection: collection, totalPage: response.totalPage, current_page:page, recordsLength: response.records.length}) );
						productList.render();
					}
				});
			} else {
				products.fetch({
					success: function(collection, response, xhr) {
						var productList = router.showView( new Application.Views.ProductListView({collection: collection, totalPage: response.totalPage, current_page:page, recordsLength: response.records.length}) );
						productList.render();
					}
				});
			}

			$('.active').removeClass('active');
			$('[href="#food"]').parent().addClass('active');
		});

		router.on('route:promoList', function(page) {
			var page = page || 1;
			var promos = new Application.Collections.Promo({page: page});
			promos.fetch({
				success: function(collection, response, xhr) {
					var promoList = router.showView( new Application.Views.PromoListView({collection: collection, totalPage: response.totalPage, current_page:page, recordsLength: response.records.length}) );
					promoList.render();
				}
			});

			$('.active').removeClass('active');
			$('[href="#promo"]').parent().addClass('active');
		});

		router.on('route:reservationList', function(status, page) {
			var page = page || 1;
			var reservations = new Application.Collections.Reservation({page: page});
			reservations.fetch({
				data: {
					status: status
				},
				success: function(collection, response, xhr) {
					var reservation = router.showView( new Application.Views.ReservationListView({collection: collection, totalPage: response.totalPage, current_page:page, recordsLength: response.records.length, status: status}) );
					reservation.render();
				}
			});

			$('.active').removeClass('active');
			$('[href="#reservation"]').parent().addClass('active');
			$('[href="#reservation/'+status+'"]').parent().addClass('active');
		});

		router.on('route:archiveList', function(page) {
			var page = page || 1;
			var archives = new Application.Collections.Archive({page: page});
			archives.fetch({
				data: {
					status: 'accepted'
				},
				success: function(collection, response, xhr) {
					var reservation = router.showView( new Application.Views.ArchiveListView({collection: collection, totalPage: response.totalPage, current_page:page, recordsLength: response.records.length, status: status}) );
					reservation.render();
				} 
			});

			$('.active').removeClass('active');
			$('[href="#transactions"]').parent().addClass('active');
			$('[href="#archive"]').parent().addClass('active');
		});

		router.on('route:report', function() {
			var report = new Application.Views.ReportView();
			report.render();

			$('.active').removeClass('active');
			$('[href="#report"]').parent().addClass('active');
		});

		router.on('route:inquiryList', function(page) {
			var page = page || 1;
			var inquiries = new Application.Collections.Inquiry({page: page});
			inquiries.fetch({
				success: function(collection, response, xhr) {
					var list = router.showView( new Application.Views.InquiryListView({collection: collection, totalPage: response.totalPage, current_page:page, recordsLength: response.records.length}) );
					list.render();
				}
			});

			$('.active').removeClass('active');
			$('[href="#inquiry"]').parent().addClass('active');
		});

		router.on('route:settings', function () {
			var reward_settings = new Application.Collections.RewardSetting();
			reward_settings.fetch({
				success : function (collection, response, xhr) {
					var settings = router.showView(new Application.Views.RewardSettingsView({model : collection.models[0]}));
					settings.render();
				}
			});
			$('.active').removeClass('active');
		});


		Backbone.history.start();
	};

	return {
		initialize: initialize
	}

});
