define([
	'backbone',
	'text!templates/pay_on_table/payOnTableListItem.html',
	'views/pay_on_table/payOnTableListItemProduct'
	], function( Backbone, payOnTableListItemView, payOnTableListItemProductView ) {
	var view = Backbone.View.extend({
		tagName: 'tr',
		events: {
			'click .actionButton' 		: 'itemStatus',
			'click .declineTransaction' : 'removeTransaction',
			'click .acceptTransaction' 	: 'acceptTransaction',
			'click .payTransaction'		: 'payTransaction'
		},
		initialize: function(options) {
			this.options = options;
			this.Unavailable = 0;
		},
		render: function() {
			this.$el.html( _.template(payOnTableListItemView, this.model.toJSON()) );
			this.addAllFood();
		},
		addAllFood: function() {
			var self = this;
			var json = this.model.get('transaction_items');

			_.each(json, function(item) {
				self.addOne(item);
			});
		},
		addOne: function(item) {
			var self = this;
			var productItem = new payOnTableListItemProductView({model: item});
			productItem.render();

			if(this.model.get('status') !== 'pending')
				$('.actionButton', productItem.$el).remove();

			$('.transactionItem', this.$el).append( productItem.$el );

			if( item.addons != undefined )
				self.foodAddOns(item.addons);
		},
		foodAddOns: function(addons) {
			var foodAddOns = "",
				self = this;

			_.each(addons, function(addOn) {
				foodAddOns = addOn.name +", "+ foodAddOns;
			});

			if(addons.length > 0) {
				$('.transactionItem', self.$el).append('<div class="row"><span class="col-md-3 col-md-offset-3 addons">Add-ons: '+foodAddOns+'</span></div>');
			}
		},
		itemStatus: function(evt) {
			evt.preventDefault();
			var target = $(evt.currentTarget);
			if(target.hasClass('btn-success')) {
				target.removeClass('btn-success').addClass('btn-danger').text('Unavailable');
				this.Unavailable++;
			} else if(target.hasClass('btn-danger')) {
				target.removeClass('btn-danger').addClass('btn-success').text('Available');
				this.Unavailable--;
			}

			if(this.Unavailable > 0) {
				$('.acceptTransaction', this.$el).addClass('disabled');
			} else {
				$('.acceptTransaction', this.$el).removeClass('disabled');
			}
		},
		removeTransaction: function(evt) {
			$('#audio').attr('src', '');
			$('#cashAudio').attr('src', '');

			evt.preventDefault();
			NProgress.start();
			var data = this.model.attributes;
			var self = this;

			delete data._id;
			data.status = 'declined';
			var archiveModel = new Application.Models.Archive(data);
			var archiveCollection = new Application.Collections.Archive({page: 1});

			$.ajax({
				type: "PUT",
				url: base_url + '/user/'+data.user_id+'/return_rewards',
				data: {
					credit: data.use_credits
				},
				success: function() {
					archiveModel.save(data, {
						success: function(result, model) {
							console.log('save in archives');
							if(self.model.destroy()) {
								$(self.$el).fadeOut();
								archiveCollection.add(model);
								NProgress.done();
								$().toastmessage('showToast', {
									text     : 'Transaction Declined',
									sticky   : false,
									type     : 'info'
								});

								if( pendingPayTableBadgeCount > 0)
									pendingPayTableBadgeCount-=1;

								$('ul.nav.navbar-nav li a[href="#pay_on_table"]').removeClass('blink');

								if( pendingPayTableBadgeCount > 0 ) {
									$('ul.nav.navbar-nav li a[href="#pay_on_table"] span.badge').text(pendingPayTableBadgeCount);
								} else {
									$('ul.nav.navbar-nav li a[href="#pay_on_table"] span.badge').text('');
								}
							}
						},
						error: function(result, response) {
							NProgress.done();
						}
					});
				},
				error: function() {
					NProgress.done();
					$().toastmessage('showToast', {
						text     : 'Failed to Decline Transaction',
						sticky   : false,
						type     : 'warning'
					});
				}
			});
		},
		acceptTransaction: function(evt) {
			$('#audio').attr('src', '');
			$('#cashAudio').attr('src', '');

			evt.preventDefault();
			NProgress.start();
			var data = this.model.attributes;
			var self = this;

			delete data._id;
			data.status = 'served';

			this.model.save(data, {
				success: function(result, response, xhr) {
					$(self.$el).fadeOut();
					NProgress.done();
					$().toastmessage('showToast', {
						text     : 'Items are served',
						sticky   : false,
						type     : 'notice'
					});

					if( pendingPayTableBadgeCount > 0)
						pendingPayTableBadgeCount-=1;

					$('ul.nav.navbar-nav li a[href="#pay_on_table"]').removeClass('blink');

					if( pendingPayTableBadgeCount > 0 ) {
						$('ul.nav.navbar-nav li a[href="#pay_on_table"] span.badge').text(pendingPayTableBadgeCount);
					} else {
						$('ul.nav.navbar-nav li a[href="#pay_on_table"] span.badge').text('');
					}
				}
			});
		},
		payTransaction: function(evt) {
			$('#audio').attr('src', '');
			$('#cashAudio').attr('src', '');

			NProgress.start();
			var data = this.model.attributes;
			var self = this;
			delete data._id;

			data.status = 'accepted';

			$.ajax({
				type: "PUT",
				url: base_url+'/user/'+data.user_id+'/increase_reward',
				data: {
					transaction_amount: data.transaction_total,
					use_credits: data.use_credits
				},
				success: function() {
					// remove and save to archive
					var archiveModel = new Application.Models.Archive(data);
					var archiveCollection = new Application.Collections.Archive({page: 1});
					archiveModel.save(data, {
						success: function(result, model) {
							if(self.model.destroy()) {

								if (self.options.status === 'cash') {
									if( servedCashPayTableBadgeCount > 0)
										servedCashPayTableBadgeCount-=1;

									if( servedCashPayTableBadgeCount > 0 ) {
										$('ul.nav.navbar-nav li a span.cashBadge').text(servedCashPayTableBadgeCount);
									} else {
										$('ul.nav.navbar-nav li a span.cashBadge').text('');
									}
								}

								$(self.$el).fadeOut();
								archiveCollection.add(model);
								NProgress.done();
								$().toastmessage('showToast', {
									text     : 'success',
									sticky   : false,
									type     : 'success'
								});
							}
						},
						error: function(result, response) {
							NProgress.done();
						}
					});
				}
			});


		}

	});

	return view;
});