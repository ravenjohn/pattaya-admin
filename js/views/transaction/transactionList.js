define(['backbone'], function(Backbone) {
	var transactionList = Backbone.View.extend({
		el: '#content-container',

		events: {

		},

		initialize: function(options) {
			this.options = options;
		},

		render: function() {
			this.$el.html( _.template(Application.Templates.TransactionList, {type: this.options.type}) );

			this.addAll();
		},

		addAll: function() {
			var self = this,
				models = this.options.collection.models;

			_.each(models, function(model) {
				self.addOne(model);
			});
			

			if(this.options.type === 'Pick up') {
				route_path = '#pickup/';
			} else if(this.options.type === 'Delivery') {
				route_path = '#delivery/';
			} else {
				route_path = '#pay_table/';
			}

			if(!models.length) $('.table tbody', this.$el).append('<tr><td>'+this.options.type+' is Empty</td></tr>');

			var pagination = new Application.Views.Pagination({currentPage: this.options.current_page, totalPage: this.options.totalPage, recordsLength: this.options.recordsLength, routePath: route_path});
			pagination.render();

			$('.transaction-pagination', this.$el).html(pagination.$el);
			var pageRoute = route_path.concat( this.options.current_page );
			$('.transaction-pagination ul>li a[href="'+pageRoute+'"]', this.$el).parent().addClass('active');
		
		},

		addOne: function(model) {
			var listItem = new Application.Views.transactionListItemView({model: model, type: this.options.type});
			listItem.render();

			$('.table tbody', this.$el).append(listItem.$el);
		}
	});

	return transactionList;
});