window.createCookie = function(cname, cvalue, days) {
	var d = new Date();
	d.setTime(d.getTime()+(days*24*60*60*1000));
	var expires = "expires="+d.toGMTString();
	document.cookie = cname + "=" + cvalue + "; " + expires;
}

window.removeCookie = function(name) {
	document.cookie = name +'=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

window.retrieveCookie = function(name) {
	var value = "; " + document.cookie;
	var parts = value.split("; " + name + "=");
	if (parts.length == 2) return parts.pop().split(";").shift();
}
