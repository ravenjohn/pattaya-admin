define(['backbone'], function(Backbone) {
	var report = Backbone.View.extend({
		el: '#content-container',

		initialize: function(options) {
			this.options = options;
		},

		render: function() {
			this.$el.html( _.template(Application.Templates.Report) );
			var date = new Date();

			var curr_day = date.getDay();
			var curr_year = date.getFullYear();
			var curr_month = date.getMonth() + 1;

			var curr_date = date.getDate();
			var start_day = curr_date - curr_day;
			var end_day = curr_date + (6 - curr_day);


			var sale = new Application.Collections.Sale(),
				self = this;

			sale.fetch({
				data: {
					status: 'accepted',
					created_month: curr_month,
					created_year: curr_year
				},
				success: function(collection, response, xhr) {
					$('#thisMonth', self.$el).text((response.total_sale).toFixed(2));
				},
				error: function() {
					console.log('error');
				}
			});

			sale.fetch({
				data: {
					status: 'accepted',
					created_month: curr_month,
					created_year: curr_year,
					start_day: start_day,
					end_day: end_day
				},
				success: function(collection, response, xhr) {
					$('#thisWeek', self.$el).text((response.total_sale).toFixed(2));
				},
				error: function() {
					console.log('error');
				}
			});

			sale.fetch({
				data: {
					status: 'accepted',
				},
				success: function(collection, response, xhr) {
					$('#totalSale', self.$el).text((response.total_sale).toFixed(2));
				},
				error: function() {
					console.log('error');
				}
			});


			// order
			var order = new Application.Collections.OrderCount();
			order.fetch({
				data: {
					status: 'accepted',
					created_month: curr_month,
					created_year: curr_year
				},
				success: function(collection, response, xhr) {
					$('#thisMonthOrder', self.$el).text(response.OrderCount);
				},
				error: function() {
					console.log('error');
				}
			});

			order.fetch({
				data: {
					status: 'accepted',
					created_month: curr_month,
					created_year: curr_year,
					start_day: start_day,
					end_day: end_day
				},
				success: function(collection, response, xhr) {
					$('#thisWeekOrder', self.$el).text(response.OrderCount);
				},
				error: function() {
					console.log('error');
				}
			});

			order.fetch({
				data: {
					status: 'accepted',
				},
				success: function(collection, response, xhr) {
					$('#totalOrder', self.$el).text(response.OrderCount);
				},
				error: function() {
					console.log('error');
				}
			});

		}
	});

	return report;
});