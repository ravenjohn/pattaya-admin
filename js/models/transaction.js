define(['backbone'], function(Backbone) {
	var transactionModel = Backbone.Model.extend({
		idAttribute:"_id",
		initialize: function(options) {
			this.options = options;
		},

		urlRoot: function() {
			return base_url + "/transaction";
		},

		defaults: {
			user_id : '',
			mode_of_payment: '',
			type: '',
			tip: 0,
			transaction_items: [],
			transaction_total: '',
			branch_name: '',
			date_receive: +new Date,
			tax: 0,
			use_credits: 0,
			transaction_total: 0,
			table_number: ""
		}
	});

	return transactionModel;
});