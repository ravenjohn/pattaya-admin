define(['backbone'], function(Backbone) {
	var promo = Backbone.Model.extend({
		idAttribute: '_id',
		initialize: function(options) {
			this.options = options;
		},
		urlRoot: function() {
			return base_url + '/promo';
		},
		validation: function() {
			return {
				promo_discount: [
				{
					pattern: 'number',
					msg: 'numeric only'
				},
				{
					required: true,
					msg: 'Required'
				},
				{
					range: [1, 100],
					msg: '1-100 only'
				}
				],
				validity: {
					required: true,
					msg: 'Validity is Required'
				}
			};
		},
		defaults : {
			promo_discount : 1,
			validity : new Date().toJSON().slice(0,10),
			name: '',
			original_price: ''
		}
	});

	return promo;
});