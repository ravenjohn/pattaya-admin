define(['backbone'], function(Backbone) {
	var orders = Backbone.Collection.extend({
		initialize: function(options) {
			this.options = options;
		},
		url: function() {
			return base_url+ "/order_count";
		}
	});

	return orders;
});