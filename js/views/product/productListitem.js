define(['backbone', 'bootbox'], function(Backbone, Bootbox) {
	var productList = Backbone.View.extend({
		tagName: 'tr',
		events: {
			'click .edit'		: 'Edit',
			'click .delete'		: 'Delete',
			'click .adOns'		: 'addOns',
			'click .promoFood'	: 'promoFood',
			'click .addChoice'	: 'addChoice'
		},
		initialize: function(options) {
			this.options = options;
			this.model.on('change', this.render, this);
		},
		
		render: function() {
			var template = _.template(Application.Templates.ProductListItem, this.model.toJSON());
			this.$el.html(template);

			this.addChoiceList();
			this.addAddOnsList();
		},

		Edit: function(evt) {
			evt.preventDefault();
			var modal = new Application.Views.Modal(this.model, { template: Application.Templates.UpdateProduct, collection: this.collection });
			modal.show();
		},

		Delete: function(evt) {
			evt.preventDefault();
			var self = this;
			bootbox.confirm("Are you sure?", function(result) {
				if(result) {
					if(self.model.destroy()) {
						$(self.$el).fadeOut();
					}
				}
			});
		},

		addOns: function(evt) {
			evt.preventDefault();
			var modal = new Application.Views.AddOnsModal(this.model, {});
			modal.show();
		},

		promoFood: function(evt) {
			evt.preventDefault();
			var model = new Application.Models.Promo({name: this.model.get('name'), food_id: this.model.get('_id'), original_price: this.model.get('price'), desription: this.model.get('description'), branch_id: this.model.get('branch_id')});
			var modal = new Application.Views.PromoModal(model, {});
			modal.show();
		},

		addChoice: function(evt) {
			evt.preventDefault();
			var choice = new Application.Views.ChoiceModal(this.model, {collection: this.collection});
			choice.show();
		},

		addChoiceList: function() {
			var self = this,
				subcategories = this.model.get('subcategories');

			var choice = new Application.Views.ChoiceListView({model: this.model ,subcategories: subcategories});
			choice.render();

			$('.ChoiceList', this.$el).html(choice.$el);
		},

		addAddOnsList: function() {
			var self = this,
				addons = this.model.get("addons");

			var addOnsList = new Application.Views.AddOnsListView({model: this.model, addons: addons});
			addOnsList.render();

			$('.AddOnsList', this.$el).html(addOnsList.$el);
		}
	});

	return productList;
});