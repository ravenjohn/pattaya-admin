define(['backbone', 'models/transaction'], function(Backbone, transactionModel) {
	var transactionCollection = Backbone.Collection.extend({
		model: transactionModel,
		initialize: function(options) {
			this.options = options;
		},
		url: function() {
			return base_url + '/transaction/?page='+ this.options.page;
		},
		parse: function(response) {
			return response.records;
		}
	});

	return transactionCollection;
});