define(['backbone'], function(Backbone) {
	var inquirylist = Backbone.View.extend({
		el: '#content-container',

		events: {

		},

		initialize: function(options) {
			this.options = options;
		},

		render: function() {
			this.$el.html( _.template(Application.Templates.InquiryList) );

			this.addAll();
		},

		addAll: function() {
			var self = this,
				models = this.options.collection.models;

			_.each(models, function(model) {
				self.addOne(model);
			});


			var pagination = new Application.Views.Pagination({currentPage: this.options.current_page, totalPage: this.options.totalPage, recordsLength: this.options.recordsLength, routePath: '#inquiry/'});
			pagination.render();

			$('.inquiry-pagination', this.$el).html(pagination.$el);
			$('.inquiry-pagination ul>li a[href="#inquiry/'+this.options.current_page+'"]', this.$el).parent().addClass('active');
		},

		addOne: function(model) {
			var item = new Application.Views.InquiryListItemView({model: model});
			item.render();

			$('.table tbody', this.$el).append(item.$el);
		}
	});

	return inquirylist;
});