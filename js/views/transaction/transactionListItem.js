define(['backbone', 'nprogress', 'toast'], function(Backbone, NProgress) {
	var transactionListItem = Backbone.View.extend({
		tagName: 'tr',

		events: {
			'click .declineTransaction' : 'removeTransaction',
			'click .acceptTransaction' 	: 'acceptTransaction',
			'click .actionButton' 		: 'itemStatus'
		},

		initialize: function(options) {
			this.options = options;
			this.Unavailable = 0;
		},

		render: function() {
			this.$el.html( _.template(Application.Templates.TransactionListItem, this.model.toJSON()) );

			this.addAll();
		},

		addAll: function() {
			var model = this.model
				self = this;

			var json = model.get('transaction_items');
			_.each(json, function(item) {
				self.addOne(item);
			});

		},

		addOne: function(food) {
			var model = food,
				self = this;
			var food = new Application.Views.TransactionProductListView({model: food});
			food.render();

			$('.transactionItem', this.$el).append( food.$el );

			if(model.addons != undefined) {
				self.food_Addons(model.addons);
			}
		},

		food_Addons: function(addons) {
			var foodAddOns = "",
				self = this;

			_.each(addons, function(addOn) {
				foodAddOns = addOn.name +", "+ foodAddOns;
			});

			if(addons.length > 0) {
				$('.transactionItem', self.$el).append('<div class="row"><span class="col-md-3 col-md-offset-3 addons">Add-ons: '+foodAddOns+'</span></div>');
			}
		},

		removeTransaction: function(evt) {
			$('#audio').attr('src', '');

			evt.preventDefault();
			NProgress.start();
			var data = this.model.attributes;
			var self = this;

			delete data._id;
			data.status = 'declined';
			var archiveModel = new Application.Models.Archive(data);
			var archiveCollection = new Application.Collections.Archive({page: 1});

			$.ajax({
				type: "PUT",
				url: base_url + '/user/'+data.user_id+'/return_rewards',
				data: {
					credit: data.use_credits
				},
				success: function() {
					archiveModel.save(data, {
						success: function(result, model) {
							console.log('save in archives');
							if(self.model.destroy()) {
								$(self.$el).fadeOut();
								archiveCollection.add(model);
								NProgress.done();
								$().toastmessage('showToast', {
									text     : 'Transaction Declined',
									sticky   : false,
									type     : 'info'
								});

								
								if(self.options.type === 'Pick up') {
									if( pendingPickupBadgeCount > 0)
										pendingPickupBadgeCount-=1;

									$('ul.nav.navbar-nav li a[href="#pickup"]').removeClass('blink');

									if( pendingPickupBadgeCount > 0 ) {
										$('ul.nav.navbar-nav li a[href="#pickup"] span.badge').text(pendingPickupBadgeCount);
									} else {
										$('ul.nav.navbar-nav li a[href="#pickup"] span.badge').text('');
									}
								} else if(self.options.type === 'Delivery') {
									if( pendingDeliveryBadgeCount > 0)
										pendingDeliveryBadgeCount-=1;
									
									$('ul.nav.navbar-nav li a[href="#delivery"]').removeClass('blink');

									if( pendingDeliveryBadgeCount > 0 ) {
										$('ul.nav.navbar-nav li a[href="#delivery"] span.badge').text(pendingDeliveryBadgeCount);
									} else {
										$('ul.nav.navbar-nav li a[href="#delivery"] span.badge').text('');
									}
								}

							}
						},
						error: function(result, response) {
							NProgress.done();
						}
					});
				},
				error: function() {
					NProgress.done();
					$().toastmessage('showToast', {
						text     : 'Failed to Decline Transaction',
						sticky   : false,
						type     : 'warning'
					});
				}
			});
		},

		acceptTransaction: function(evt) {
			$('#audio').attr('src', '');

			evt.preventDefault();
			var thisBtn = $(evt.currentTarget, this.$el);
			if(!thisBtn.hasClass('disabled'))
				thisBtn.addClass('disabled');

			var model = this.model;
			var data = model.attributes;
			var self = this;

			delete data._id;
			data.status = 'accepted';

			var archiveModel = new Application.Models.Archive(data);
			var archiveCollection = new Application.Collections.Archive({page: 1});

			NProgress.start();

			if(model.get('mode_of_payment') === 'cash') {
				$.ajax({
					type: "PUT",
					url: base_url+'/user/'+data.user_id+'/increase_reward',
					data: {
						transaction_amount: data.transaction_total,
						use_credits: data.use_credits
					},
					success: function() {
						archiveModel.save(data, {
							success: function(result, model) {
								if(self.model.destroy()) {
									$(self.$el).fadeOut();
									archiveCollection.add(model);
									NProgress.done();
									$().toastmessage('showToast', {
										text     : 'Transaction Accepted - CASH',
										sticky   : false,
										type     : 'success'
									});

									console.log('transaction Accepted.. method of payment Cash');

									if(thisBtn.hasClass('disabled'))
										thisBtn.removeClass('disabled');



									//Badge
									if(self.options.type === 'Pick up') {
										if(pendingPickupBadgeCount > 0)
											pendingPickupBadgeCount-=1;
										$('ul.nav.navbar-nav li a[href="#pickup"]').removeClass('blink');

										if( pendingPickupBadgeCount > 0 ) {
											$('ul.nav.navbar-nav li a[href="#pickup"] span.badge').text(pendingPickupBadgeCount);
										} else {
											$('ul.nav.navbar-nav li a[href="#pickup"] span.badge').text('');
										}
									} else if(self.options.type === 'Delivery') {
										if( pendingDeliveryBadgeCount > 0)
											pendingDeliveryBadgeCount-=1;

										$('ul.nav.navbar-nav li a[href="#delivery"]').removeClass('blink');

										if( pendingDeliveryBadgeCount > 0 ) {
											$('ul.nav.navbar-nav li a[href="#delivery"] span.badge').text(pendingDeliveryBadgeCount);
										} else {
											$('ul.nav.navbar-nav li a[href="#delivery"] span.badge').text('');
										}
									}



								}
							},
							error: function(result, response) {
								NProgress.done();
								if(thisBtn.hasClass('disabled'))
									thisBtn.removeClass('disabled');

								console.log('transaction Accepted.. method of payment Cash but something went wrong... archiveModel fail to save');
							}
						});
					},
					error: function() {
						NProgress.done();
						$().toastmessage('showToast', {
							text     : 'Failed',
							sticky   : false,
							type     : 'warning'
						});
					}
				});
			}
			else if (model.get('mode_of_payment') === 'stripe') {
				$.ajax({
					type :"post",
					data: {
						amount: model.get('transaction_total'),
						use_credits: model.get('use_credits')
					},
					url: base_url +'/user/' + model.get('user_id') + '/stripe/charge',
					success: function(data, response, XHR) {
						if(data.Error) {
							NProgress.done();
							$().toastmessage('showToast', {
								text     : 'Payment Failed',
								sticky   : false,
								type     : 'error'
							});

							if(thisBtn.hasClass('disabled'))
								thisBtn.removeClass('disabled');
						} else {
							archiveModel.save(data, {
								success: function(result, model) {
									archiveCollection.add(model);
									if(self.model.destroy()) {
										$(self.$el).fadeOut();
										NProgress.done();
										$().toastmessage('showToast', {
											text     : 'Payment Successful',
											sticky   : false,
											type     : 'success'
										});

										if(thisBtn.hasClass('disabled'))
											thisBtn.removeClass('disabled');

										//Badge
										if(self.options.type === 'Pick up') {
											if( pendingPickupBadgeCount > 0)
												pendingPickupBadgeCount-=1;

											$('ul.nav.navbar-nav li a[href="#pickup"]').removeClass('blink');

											if( pendingPickupBadgeCount > 0 ) {
												$('ul.nav.navbar-nav li a[href="#pickup"] span.badge').text(pendingPickupBadgeCount);
											} else {
												$('ul.nav.navbar-nav li a[href="#pickup"] span.badge').text('');
											}
										} else if(self.options.type === 'Delivery') {
											if( pendingDeliveryBadgeCount > 0 )
												pendingDeliveryBadgeCount-=1;

											$('ul.nav.navbar-nav li a[href="#delivery"]').removeClass('blink');

											if( pendingDeliveryBadgeCount > 0 ) {
												$('ul.nav.navbar-nav li a[href="#delivery"] span.badge').text(pendingDeliveryBadgeCount);
											} else {
												$('ul.nav.navbar-nav li a[href="#delivery"] span.badge').text('');
											}
										}

									}
								},
								error: function(result, response) {
									NProgress.done();
									if(thisBtn.hasClass('disabled'))
										thisBtn.removeClass('disabled');
								}
							});
						}
					},
					error: function(err) {
						$(self.$el).fadeOut();
						NProgress.done();
						$().toastmessage('showToast', {
							text     : 'Payment Failed',
							sticky   : false,
							type     : 'warning'
						});

						console.log('STRIPE PAYMENT FAILED....');
						if(thisBtn.hasClass('disabled'))
							thisBtn.removeClass('disabled');
					}
				});
			} else {
				$.ajax({
					type: "PUT",
					url: base_url+'/user/'+data.user_id+'/increase_reward',
					data: {
						transaction_amount: data.transaction_total,
						use_credits: data.use_credits
					},
					success: function() {
						$.ajax({
							type :"post",
							data: {
								amount: model.get('transaction_total')
							},
							url: base_url +'/payment/'+ model.get('user_id'),
							success: function(data, response, XHR) {
								//payment success..
								if(data.Error) {
									NProgress.done();
									$().toastmessage('showToast', {
										text     : 'Payment Failed',
										sticky   : false,
										type     : 'error'
									});

									if(thisBtn.hasClass('disabled'))
										thisBtn.removeClass('disabled');
								} else {
									archiveModel.save(data, {
										success: function(result, model) {
											archiveCollection.add(model);
											if(self.model.destroy()) {
												$(self.$el).fadeOut();
												NProgress.done();
												$().toastmessage('showToast', {
													text     : 'Payment Successful',
													sticky   : false,
													type     : 'success'
												});

												if(thisBtn.hasClass('disabled'))
													thisBtn.removeClass('disabled');

												//Badge
												if(self.options.type === 'Pick up') {
													if(pendingPickupBadgeCount > 0)
														pendingPickupBadgeCount-=1;

													$('ul.nav.navbar-nav li a[href="#pickup"]').removeClass('blink');

													if( pendingPickupBadgeCount > 0 ) {
														$('ul.nav.navbar-nav li a[href="#pickup"] span.badge').text(pendingPickupBadgeCount);
													} else {
														$('ul.nav.navbar-nav li a[href="#pickup"] span.badge').text('');
													}
												} else if(self.options.type === 'Delivery') {
													if(pendingDeliveryBadgeCount > 0)
														pendingDeliveryBadgeCount-=1;

													$('ul.nav.navbar-nav li a[href="#delivery"]').removeClass('blink');

													if( pendingDeliveryBadgeCount > 0 ) {
														$('ul.nav.navbar-nav li a[href="#delivery"] span.badge').text(pendingDeliveryBadgeCount);
													} else {
														$('ul.nav.navbar-nav li a[href="#delivery"] span.badge').text('');
													}
												}
											}
										},
										error: function(result, response) {
											NProgress.done();
											if(thisBtn.hasClass('disabled'))
												thisBtn.removeClass('disabled');
										}
									});
								}

							},
							error: function(err) {
								console.log('PAYPAL PAYMENT FAILED....');
								if(thisBtn.hasClass('disabled'))
									thisBtn.removeClass('disabled');
							}
						});

					},
					error: function() {
						NProgress.done();
						$().toastmessage('showToast', {
							text     : 'Failed',
							sticky   : false,
							type     : 'warning'
						});
					}
				});
			}
		},

		itemStatus: function(evt) {
			evt.preventDefault();
			var target = $(evt.currentTarget);
			if(target.hasClass('btn-success')) {
				target.removeClass('btn-success').addClass('btn-danger').text('Unavailable');
				this.Unavailable++;
			} else if(target.hasClass('btn-danger')) {
				target.removeClass('btn-danger').addClass('btn-success').text('Available');
				this.Unavailable--;
			}

			if(this.Unavailable > 0) {
				$('.acceptTransaction', this.$el).addClass('disabled');
			} else {
				$('.acceptTransaction', this.$el).removeClass('disabled');
			}
		}
	});

	return transactionListItem;
});
