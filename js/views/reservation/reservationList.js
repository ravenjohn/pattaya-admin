define(['backbone'], function(Backbone) {
	var reservationList = Backbone.View.extend({
		el: '#content-container',
		events: {
			'click .create'		: 'create'
		},
		initialize: function(options) {
			this.options = options;
			var self = this;
			this.collection.on('add', function(model, collection, options) {
				if(window.location.hash === '#reservation/accepted')
					self.addOne(model);
			});
		},
		render: function() {
			this.$el.html( _.template(Application.Templates.ReservationList) );

			this.addAll();
		},

		addAll: function() {
			var self = this,
				models = this.options.collection.models;

			_.each(models, function(model) {
				self.addOne(model);
			});

			var pagination = new Application.Views.Pagination({currentPage: this.options.current_page, totalPage: this.options.totalPage, recordsLength: this.options.recordsLength, routePath: '#reservation/'});
			pagination.render();

			$('.reservation-pagination', this.$el).html(pagination.$el);
			$('.reservation-pagination ul>li a[href="#reservation/'+this.options.current_page+'"]', this.$el).parent().addClass('active');
		},

		addOne: function(model) {
			var self = this,
				item = new Application.Views.ReservationListItemView({model: model}),
				status = this.options.status;
			item.render();


			switch (status) {
				case 'pending':
					$('.done', item.$el).hide();
					break;
				case 'accepted':
					$('.accept', item.$el).hide();
					$('.decline', item.$el).text('Remove');
					break;
				case 'done':
					$('.accept', item.$el).hide();
					$('.decline', item.$el).hide();
					$('.done', item.$el).hide();
					break;
			}

			$('.table tbody', this.$el).append(item.$el);
		},
 
		create: function(evt) {
			evt.preventDefault();
			
			var model = new Application.Models.Reservation();
			var modal = new Application.Views.Modal(model, {template: Application.Templates.UpdateReservation, collection: this.collection});
			modal.show();
		}
	});

	return reservationList;
});