define(['backbone', 'models/reward_setting'], function(Backbone, rewardSettingModel) {
	var rewardSettingsCollection = Backbone.Collection.extend({
		model : rewardSettingModel,
		initialize: function(options) {
			this.options = options;
		},
		url: function() {
			return base_url+"/reward_setting";
		}
	});

	return rewardSettingsCollection;
});
