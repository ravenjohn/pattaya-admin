define(['backbone', 'models/inquiry'], function(Backbone, inquieryModel) {
	var inquiryCollection = Backbone.Collection.extend({
		model: inquieryModel,
		initialize: function(options) {
			this.options = options;
		},
		url: function() {
			return base_url+"/inquiry/?page="+this.options.page;
		},
		parse: function(response) {
			return response.records;
		}
	});

	return inquiryCollection;
});